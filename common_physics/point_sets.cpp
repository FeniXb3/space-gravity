/** @file point_sets.cpp
 *  @brief implementation of different point sets
 *
 *	@author Bartlomiej Filipek
 *	@date April 2011
 */

#include "stdafx.h"
#include "material_point.h"
#include "point_sets.h"

///////////////////////////////////////////////////////////////////////////////
// StarsPointSet
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
StarsPointSet::StarsPointSet(unsigned int count) : MaterialPointSet(count)
{

}


///////////////////////////////////////////////////////////////////////////////
bool StarsPointSet::CollisionCheck(unsigned int i)
{
	bool retval = false;
	
	for(int j = 0; j < m_count; j++)
	{
		if(j != i)
		{
			Vec3d deltapos = m_points[i].m_pos - m_points[j].m_pos;
			double len = deltapos.Length();
			
			if(len < m_points[i].m_radius + m_points[j].m_radius)
			{
				retval = true;
			}
		}
	}

	return retval;
}

///////////////////////////////////////////////////////////////////////////////
Vec3d StarsPointSet::Force(unsigned int i)
{
	Vec3d force(0.0);

	if(!m_points[i].movable)
		return force;

	double G = 0.00000000006673848;

	for(int j = 0; j < m_count; j++)
	{
		if(j != i)
		{
			Vec3d deltapos = m_points[i].m_pos - m_points[j].m_pos;
			double len = deltapos.Length();
			Vec3d tmpForce(0);
			
			tmpForce = G*(m_points[i].m_mass*m_points[j].m_mass)/(len*len);
			
			force.x -= tmpForce.x * (deltapos.x/len);
			force.y -= tmpForce.y * (deltapos.y/len);
			force.z -= tmpForce.z * (deltapos.z/len);
		}
	}
	
	return force;
}

