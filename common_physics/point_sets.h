/** @file point_sets.h
 *  @brief declaration of different point sets
 *
 *	@author Bartlomiej Filipek
 *	@date April 2011
 */

#pragma once

#include <assert.h>
#include "utils_math.h"
#include "material_point.h"

///////////////////////////////////////////////////////////////////////////////
class StarsPointSet : public MaterialPointSet
{
public:
public:
	StarsPointSet(unsigned int count);
	~StarsPointSet() { }

	virtual Vec3d Force(unsigned int i);
	virtual bool CollisionCheck(unsigned int i);
};




