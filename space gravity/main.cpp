#include "stdafx.h"
#include "utils.h"
#include "utils_shaders.h"
#include "utils_math.h"

#include "input.h"

#include "material_point.h"
#include "point_sets.h"

#pragma region globals
///////////////////////////////////////////////////////////////////////////////
// globals
Camera g_camera;

// shaders:
GLuint g_shaderLight;

// geometry to render
GLint g_meshDisplayList;

// time:
double g_appTime = 0.0;	// global app time in seconds

bool m_isLeftPressed = false;
int newIndex = -1;
double points = 0;
bool gameOver = false;

// menu:
#define MENU_EXIT 1
#define MENU_RESET 2
#define MENU_EULER 3
#define MENU_VERLET 4

#pragma endregion

MaterialPoint testPoint;

//MaterialPointSet g_universe(11);

StarsPointSet g_universe(2);

// LinePointSet g_line(11);

///////////////////////////////////////////////////////////////////////////////
// function prototypes
bool InitApp();
void InitSimulation();
void ResetSimulation();
void CleanUp();

// callbacks:
void ChangeSize(int w, int h);

// keyboard:
void ProcessNormalKeys(unsigned char key, int x, int y);
void PressSpecialKey(int key, int x, int y);
void ReleaseSpecialKey(int key, int x, int y);

// mouse:
void ProcessMouse(int button, int state, int x, int y);
void ProcessMouseMotion(int x, int y);
void ProcessMouseWheel(int button, int dir, int x, int y);


void Idle();
void UpdateScene(double deltaSeconds);
void RenderScene();

///////////////////////////////////////////////////////////////////////////////
// entry point
int main(int argc, char **argv)
{
	// init GLUT
	glutInit(&argc, argv);
	// use depth buffer, double buffering and standard RGBA mode
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(200,200);
	// standard resolution:
	glutInitWindowSize(640, 480);
	// title:
	glutCreateWindow("<add title>");

	// set callbacks:
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(ProcessNormalKeys);
	glutSpecialFunc(PressSpecialKey);
	glutSpecialUpFunc(ReleaseSpecialKey);
	glutMouseFunc(ProcessMouse);
	glutMotionFunc(ProcessMouseMotion);
	glutDisplayFunc(RenderScene);
	glutIdleFunc(Idle);
	glutReshapeFunc(ChangeSize);

	glutMouseWheelFunc(ProcessMouseWheel);
	
	// init OpenGL extensions (like shader support, framebuffer object, etc)
	if (utInitOpenGL20() == false) return 1;

	// init whole application:
	if (InitApp() == false)
	{
		utLOG_ERROR("cannot init application...");
		return 1;
	}

	// start rendering and animation:
	glutMainLoop();

	// finish:
	CleanUp();

	return(0);
}

#pragma region Init & CleanUp
///////////////////////////////////////////////////////////////////////////////
bool InitApp() 
{	
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_DEPTH);
	
	srand(time(NULL));

	if (utLoadAndBuildShader("data\\light.vert", "data\\light.frag", &g_shaderLight) == false)
		return false;

	g_camera.m_zoom = 10.0f;

	InitSimulation();

	return true;
}

void InitSimulation()
{
	ResetSimulation();
}

void ResetSimulation()
{
	gameOver = false;
	g_appTime = 0;
	points = 0;
	g_universe.Reset();

	for (unsigned int i = 0; i < g_universe.PointCount(); ++i)
	{
		if(i >= g_universe.PointCount() -1)
		{
			int tmpDivider = g_universe.PointCount() - i;

			g_universe.Point(i)->m_pos = Vec3d(0,0,0);//120/tmpDivider, 140/tmpDivider, 0.0f);
			g_universe.Point(i)->m_vel = Vec3d(0.0, 0.0, 0.0);
			g_universe.Point(i)->m_mass = 99999999999;
			g_universe.Point(i)->m_radius = 0.2* (i+1);;
			g_universe.Point(i)->Reset();
			g_universe.Point(i)->movable = false;
		}
		else
		{
			g_universe.Point(i)->m_pos = Vec3d(3,3,0);//Rand<double>(1, 10), Rand<double>(1, 10), 0.0f);
			g_universe.Point(i)->m_vel = Vec3d(0.0, 0.0, 0.0);
			g_universe.Point(i)->m_mass = 999*(i)+ 10;
			g_universe.Point(i)->m_radius = 0.2* (i+1);
			g_universe.Point(i)->Reset();

			g_universe.Point(i)->m_vel = Vec3d(-0.7, 0.7, 0.0);
			


			Vec3d planetToStar = Vec3d(0,0,0) - g_universe.Point(i)->m_pos;
			Vec3d basicVec(1, planetToStar.x/planetToStar.y, 0);

			 basicVec.Normalize();
			//g_universe.Point(i)->m_vel = basicVec*planetToStar.y;// * planetToStar.Length()/2;//Vec3d(-5.0, 8.0, 0.0);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
void CleanUp()
{
	glDeleteLists(g_meshDisplayList, 1);
	utDeleteAllUsedShaders();
}

#pragma endregion

#pragma region Input

///////////////////////////////////////////////////////////////////////////////
void ChangeSize(int w, int h)
{
	g_camera.ChangeViewportSize(w, h);
}

///////////////////////////////////////////////////////////////////////////////
void ProcessNormalKeys(unsigned char key, int x, int y) {

	if (key == 27) 
		exit(0);
	else if (key == ' ')
	{
		g_camera.m_angleX = 0.0f;
		g_camera.m_angleY = 0.0f;
		g_camera.m_zoom = 10.0f;
	}
	else if (key == 'r')
	{
		ResetSimulation();
	}
}

///////////////////////////////////////////////////////////////////////////////
void PressSpecialKey(int key, int x, int y) 
{
	g_camera.PressSpecialKey(key, x, y);
}


///////////////////////////////////////////////////////////////////////////////
void ReleaseSpecialKey(int key, int x, int y) 
{
	g_camera.ReleaseSpecialKey(key, x, y);
}

///////////////////////////////////////////////////////////////////////////////
void ProcessMouse(int button, int state, int x, int y)
{
	if(gameOver)
		return;

	if (button == GLUT_LEFT_BUTTON)
	{		
		if (state == GLUT_DOWN)
		{
			m_isLeftPressed = true;

			float fx = 0.0f, fy = 0.0f;

			fx = x - 640/2;
			fy = y - 480/2;

			fx /= 10;
			fy /= 10;
			
			fx *= 0.17;
			fy *= 0.17;

			fy = -fy;

			newIndex =  g_universe.AddPoint(Vec3d(fx,fy,0));
			
			g_universe.Point(newIndex)->m_pos = Vec3d(fx,fy,0);
			g_universe.Point(newIndex)->m_vel = Vec3d(0,0,0);
			g_universe.Point(newIndex)->m_acceleration = Vec3d(0,0,0);

			g_universe.Point(newIndex)->movable = false;
		}
		else if (state == GLUT_UP)
		{
			newIndex = g_universe.PointCount()-1;
			
			g_universe.Point(newIndex)->movable = true;

			m_isLeftPressed = false;
			newIndex = -1;
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
void ProcessMouseMotion(int x, int y)
{
	if(gameOver)
		return;

	if(!m_isLeftPressed)
		return;

	float fx = 0.0f, fy = 0.0f;

	fx = x - 640/2;
	fy = y - 480/2;

	fx /= 10;
	fy /= 10;
			
	fx *= 0.17;
	fy *= 0.17;

	fy = -fy;

	newIndex = g_universe.PointCount()-1;

	g_universe.Point(newIndex)->m_acceleration = Vec3d(0,0,0);
	g_universe.Point(newIndex)->m_vel = Vec3d(
		g_universe.Point(newIndex)->m_pos.x - fx,
		g_universe.Point(newIndex)->m_pos.y - fy,
		0);
}

//////////////////
void ProcessMouseWheel(int button, int dir, int x, int y)
{
	if(gameOver)
		return;

	if(m_isLeftPressed)
	{
		if(dir > 0)
		{
			g_universe.Point(newIndex)->m_radius += 0.02;
			g_universe.Point(newIndex)->m_mass += 10000000000;
	
		}
		else 
		{
			if(g_universe.Point(newIndex)->m_radius > 0.2)
			{
				g_universe.Point(newIndex)->m_radius -= 0.02;
				g_universe.Point(newIndex)->m_mass -= 10000000000;
			}
	
		}
	}
}
#pragma endregion

#pragma region Update & Render

///////////////////////////////////////////////////////////////////////////////
void Idle()
{
	static double lastDeltas[3] = { 0.0, 0.0, 0.0 };
	static const double REFRESH_TIME = 1.0/60.0;	// monitor with 60 Hz
	
	// in milisec
	int t = glutGet(GLUT_ELAPSED_TIME);
	double newTime = (double)t*0.001;

	double deltaTime = newTime - g_appTime;
	if (deltaTime > REFRESH_TIME) deltaTime = REFRESH_TIME;

	// average:
	deltaTime = (deltaTime + lastDeltas[0] + lastDeltas[1] + lastDeltas[2]) * 0.25;



	if(g_universe.PointCount() -1 == 0)
		gameOver = true;
	else
	{
		g_appTime = g_appTime+deltaTime;
		points += (g_universe.PointCount()-1) * deltaTime;
		// call Update:
		UpdateScene(deltaTime);
	}
	// render frame:
	RenderScene();

	// save delta:
	lastDeltas[0] = lastDeltas[1];
	lastDeltas[1] = lastDeltas[2];
	lastDeltas[2] = deltaTime;
}

///////////////////////////////////////////////////////////////////////////////
void UpdateScene(double deltaTime) 
{
	g_universe.Update(deltaTime);
	
	g_camera.Update(deltaTime);
}

///////////////////////////////////////////////////////////////////////////////
void RenderScene() 
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// setup camera:
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	g_camera.SetSimpleView();


	// render something
	glUseProgram(g_shaderLight);
	//float col = 0.5f+0.5f*sinf(meshAnim);
	//glColor3f(col, 0.0f, 1.0f-col);

	for (unsigned int i = 0; i < g_universe.PointCount(); ++i)
	{
		glPushMatrix();
		
			if(!g_universe.Point(i)->movable)
				glColor3f(1.0f, 1.0f, 0.0f);
			else
				glColor3f(0.5f, 0.5f, 0.7f);
			
			glTranslatef(g_universe.Point(i)->m_pos.x, g_universe.Point(i)->m_pos.y, g_universe.Point(i)->m_pos.z);
			glutWireSphere(g_universe.Point(i)->m_radius, 12, 12);
			
			glBegin(GL_LINES);
				glVertex3f(0, 0, 0);
				glVertex3f(g_universe.Point(i)->m_vel.x, g_universe.Point(i)->m_vel.y, 0);
			glEnd();

		glPopMatrix();
	}

	glUseProgram(0);

	
	char tmpstr[20];

	// draw text
	glColor3f(0.0f,1.0f,0.0f);
	utSetOrthographicProjection(g_camera.m_screenWidth, g_camera.m_screenHeight);
		
		if(!gameOver)
		{
			sprintf(tmpstr, "Time: %fs", g_appTime);
			utDrawText2D(10,35, tmpstr);
			sprintf(tmpstr, "Planets: %d", g_universe.PointCount()-1);
			utDrawText2D(10,50, tmpstr);
			sprintf(tmpstr, "Points: %d", (int)points);
			utDrawText2D(10,65, tmpstr);
		}
		utDrawText2D(10,85, "LMB - Create planet");
		utDrawText2D(10,100, "LMB + MOVE - Planet with base force");
		utDrawText2D(10,115, "LMB + WHEEL - Anjust planet size");
		utDrawText2D(10,130, "Esc - Quit");
		utDrawText2D(10,145, "R - Reset");


		if(gameOver)
		{
			utDrawText2D(200,230, "GAME OVER");
			sprintf(tmpstr, "Time: %fs", g_appTime);
			utDrawText2D(200,245, tmpstr);
			sprintf(tmpstr, "Points: %d", (int)points);
			utDrawText2D(200,260, tmpstr);
		}
	utResetPerspectiveProjection();

	glutSwapBuffers();
}

#pragma endregion

